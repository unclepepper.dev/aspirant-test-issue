<?php
namespace App\EventListener;


use Twig\Environment;

class TwigEventListener
{
    
    public function __construct(
        private Environment $twig
        )
    {}

    public function onKernelController()
    {
         $this->twig->addGlobal('time', new \DateTime('now'));
    }
}
